# Draft_WaitingLine_RdvList

# What for
Sort a list of rdv for a waiting Line

Requirements:
  -Node version 14.8.0
  -Local MongoDB Database

# Commands
Usage: 
    add --type=['immediate', 'futur'] --at=DD-MM-YYYY.HH:mm 
    get --filter=['all', 'DD-MM-YYYY'] 
    watch 
    queue
COMMANDS:
   -add: To add a new RDV
   -get: To display RDVs
   -watch: To add a link
   -queue: To sort the rdvs
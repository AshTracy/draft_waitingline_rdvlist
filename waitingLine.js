const moment = require("moment");
const fs = require("fs");
const { orderBy } = require("lodash");
module.exports = {
  inputs: {
    getRdv: {
      rdvType: "table",
    }
  },
  exits: {},

  fn: async function listAlog(inputs) {
    //GET TODAY RVDS
    let Rdvs = inputs.getRdv;
    
    let dayRdvs = Rdvs.filter((item) => {
      let date = moment().format('DD-MM-YYYY');
       if (
         moment(
           moment(item.consultation.startTimeRdv).format("DD-MM-YYYY"),
           "DD-MM-YYYY"
         )
           .utc(true)
           .toISOString() === moment(date, 'DD-MM-YYYY').utc(true).toISOString()) {
         return item;
       }
    });

    dayRdvs.forEach((r) => {
      if (r.consultation.startTimeRdv) {
      /*console.log(
        "Initial list of Rdvs ==> ",
        `startTime => ${r.consultation.startTimeRdv}`
      )*/
      r.consultation.startTimeRdv = moment(r.consultation.startTimeRdv)
        .valueOf();
      }
      /*console.log(
        "Before sort startTimeRdv ==> ",
        `startTime => ${r.consultation.startTimeRdv}`
      );*/
    });

    dayRdvs = orderBy(dayRdvs, ["consultation.startTimeRdv"], ["asc"]);

    //const dayRdvs = await RdvConsultation.find(criteria);
    dayRdvs.forEach((r) => {
        r.consultation.startTimeRdv = moment(r.consultation.startTimeRdv).toISOString();
        /*console.log(
          "SortedBy by startTimeRdv list of RDVs ==> ",
          `startTime => ${r.consultation.startTimeRdv}`
        );*/
    });
    //GET TODAY RVDS

    //GET RDVS NOT PASSED YET
    let pendingRdvs = [];
    for (const r of dayRdvs) {
      //SET PRIORITIES
      //console.log('r =>', r)
        if (r.consultation.status === "PENDING") {
        if (
          r.consultation.rdvType === "FUTUR") { 
          r.consultation.position.priority = 0;
        } else {
          r.consultation.position.priority = 1;
        }
        pendingRdvs.push(r);
        //SET PRIORITIES
      }
    }
    pendingRdvs.forEach((r) => {
      //r.startTimeRdv = moment(r.startTimeRdv).utc();
      /*console.log(
        "List of RDVs and priority ==> ",
        `startTime => ${r.consultation.startTimeRdv}, priority => ${r.consultation.position.priority}`
      );*/
    });
    //GET RDVS NOT PASSED YET

    //GET THE NEXT NUMBER
    /*const maxPositionNumber = Math.max.apply(
      Math,
      dayRdvs.map((r) => r.consultation.positionNumber)
    );
    //console.log("maxPositionNumber ==> ", maxPositionNumber);
    let positionNumber = maxPositionNumber;*/
    positionNumber = 1;
    //GET THE NEXT NUMBER

    //SORT BY PRIORITY
      //console.log("positionNumber ==> ", positionNumber, pendingRdvs);

    for (const r of pendingRdvs) {
      let lenghtN = 0;
      let sameHourRdvs = [];
      let currentTime = new Date().toISOString();

      dayRdvs = orderBy(dayRdvs, ["consultation.position.priority"], ["desc"]);
      for (const p of dayRdvs) {
        if (
          r.consultation.startTimeRdv === p.consultation.startTimeRdv &&
          r.consultation.status === "PENDING" &&
          p.consultation.createdAt !== currentTime
        ) {
          //console.log(p.consultation.startTimeRdv, currentTime);
          sameHourRdvs.unshift(p);
          lenghtN++;
        }
        currentTime = r.consultation.createdAt;
      }
      //console.log('lentghtN', lenghtN)
      if (lenghtN > 1) {
        sameHourRdvs.forEach((r) => {
          /*console.log(
            "sameHourRdvs ====>",
            ` ${r.consultation.position.priority}`
          );*/
        });
        let copy = pendingRdvs.slice(0, pendingRdvs.indexOf(r));
        let secondCopy = pendingRdvs.slice(
          pendingRdvs.indexOf(r) + (lenghtN)
        );
        copy = copy.concat(sameHourRdvs);
        copy = copy.concat(secondCopy);
        pendingRdvs = copy;
      }
    }
    //SORT BY PRIORITY

    //GIVE THE NUMBER POSITION
    let finalRdvs = [];
    //console.clear()
    for (const r of pendingRdvs) {
      r.consultation.positionNumber = positionNumber;
      /*const rdv = await RdvConsultation.updateOne({ id: r.id }).set({
        positionNumber,
      });*/
      /*
      console.log(
        "FinalRdvsList ==> ",
        `Name => ${r.consultation.startTimeRdv}, priority => ${r.consultation.position.priority} positionNumber => ${r.consultation.positionNumber}`
      );
      */
      finalRdvs.push(`${r.consultation.name} ${r.consultation.position.hour} #${r.consultation.positionNumber}`);
      console.log(finalRdvs[positionNumber - 1]);
      positionNumber++;
    }
    /*console.log(` ******************************** Final Sorted Rdv Waiting Line for ${moment().format("DD-MM-YYYY")} =>`, pendingRdvs);*/
    fs.writeFileSync("data.json", JSON.stringify(Rdvs));
    return finalRdvs;
    //GIVE THE NUMBERS POSITION
  },
};

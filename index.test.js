const database = require('./rdvs')
const sort = require('./waitingLine')
const moment = require('moment')

test('test get all rdvs command', () => {
    expect(database.filterDate({rdvDate: 'all'})).toBeDefined()
} )
test('test get rdvs of 03/11/2021', () => {
    expect(database.filterDate({rdvDate: '03/11/2021'})).toBeDefined()
} )
test('test sorting rdvs of the day', () => {
    expect(sort.fn({getRdv: database.getData()})).toBeDefined()
} )
test('test add rdv of type immediate', () => {
    expect(database.addData({rdvType: 'immediate'})).toBeDefined()
} )
test('test add rdv of type future for tomorrow', () => {
    expect(database.addData({rdvType: 'future', rdvDate: moment().add(1, 'days').utc(true).format('DD-MM-YYYY')})).toBeDefined()
})

test('test error case of get [no --filter property]', () => {
    expect(database.filterDate({rdvDate: ''})).toEqual(-1)
})
test('test error case of add [no --type property]', () => {
    expect(database.addData({rdvDate: '03/11/2021'})).toEqual(-1)
})
test('test error case of add [no --at property for future rdv]', () => {
    expect(database.addData({rdvType: 'future', rdvDate: ''})).toEqual(-1)
})
test('test error case of get [bad date input]', () => {
})

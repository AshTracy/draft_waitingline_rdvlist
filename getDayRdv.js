const { upperCase } = require("lodash");
const moment = require('moment')
const axios = require('axios');

module.exports = {
  inputs: {
    rdvDate: {
      type: "ref",
      columnType: "datetime",
    },
    rdvType: {
      type: "string",
      isIn: ["immediate", "futur"],
	},
	filter: {
		type: 'string'
	},
	watch: {
		type: 'string'
	}
  },
	exits: {},
	get: async function getRdvs({ rdvDate, filter, watch }) {
		let collection = [];

		try {
			await client.connect();
			if (!client.db("RdvTest")) {
				client.db("RdvTest").createCollection("consultation");
				console.log("created RdvTest");
			}
			await listRdvs(client)
		} catch (e) {
			console.log(error)
		}
		finally {
			await client.close()
		}

		async function listDatabases(client) {
			let databasesList = await client.db().admin().listDatabases();
		}

		async function listRdvs(client) {
			let rdvsList = [];
			if (rdvDate) {
				rdvsList = await client.db("RdvTest").collection("consultation").find({dateRdv: moment(rdvDate, 'DD-MM-YYYY').utc(true).toDate().toISOString()})
			} else {
				rdvsList = await client.db('RdvTest').collection('consultation').find({})
			}
			const results = await rdvsList.toArray()
			
			if (results.length > 0) {
				collection = results
			}
		}
		if (filter) {
			console.log("Collection RDVs:\n", collection)
		}
			axios.post("http://127.0.0.1:3000/info", { collection }).then((response) => {
				//console.log('response =>', response)
			});
			return (collection)
	},
	fn: async function createRdvs(inputs) {
		try {
			await client.connect();
			await addRdv(client)
		} catch (e) {
			console.log(error);
		} finally {
			await client.close();
		}
		async function addRdv(client) {
			let rdv =
			{
				consultation: {
					createdAt: moment().utc().toISOString(),
					updatedAt: moment().utc().toISOString(),
					dateRdv: moment(moment().format('DD-MM-YYYY'), 'DD-MM-YYYY').utc(true).toISOString(),
					positionNumber: null,
					status: "PENDING",
					startTimeRdv: moment().utc().toISOString(),
					endTimeRdv: moment().utc().toISOString(),
					payment: {
						paymentMode: "PAY_NOW",
						paymentType: "CASH",
					},
					paymentStatus: "PENDING",
					rdvType: "IMMEDIATE",
					position: {}
				}
			}
			console.log("Rdv in creation =>", rdv)
			if (inputs.rdvType) {
				rdv.consultation.rdvType = upperCase(inputs.rdvType);
			} else {
				console.log('Put a type !')
				return this.exits({ error: "Put a type !" });
			}
			if ( inputs.rdvDate && inputs.rdvType === "futur") {
				rdv.consultation.startTimeRdv = moment(inputs.rdvDate, 'DD-MM-YYYY.HH:mm')
					.utc(true)
					.toISOString();
				rdv.consultation.endTimeRdv = moment(inputs.rdvDate, 'DD-MM-YYYY.HH:mm').utc(true).toISOString();
				rdv.consultation.dateRdv = moment(moment(inputs.rdvDate, 'DD-MM-YYYY.HH:mm').format('DD-MM-YYYY'), 'DD-MM-YYYY').utc(true).toISOString();
				rdv.consultation.position.hour = moment(inputs.rdvDate, 'DD-MM-YYYY.HH:mm').format('HH:mm')
				console.log(
					`DATES => endTime => ${rdv.consultation.endTimeRdv}, dateRdv => ${rdv.consultation.dateRdv}, hour => ${rdv.consultation.position.hour}`
				);
			} else if (inputs.rdvType !== "immediate"){
				console.log("Put a date")
				return this.exits({ error: "Put a date !" });
			}

			let collection = await client
          		.db("RdvTest")
          		.collection("consultation")
				.insertOne(rdv.consultation);
				axios.post("http://127.0.0.1:3000/info", { rdv }).then((response) => {
						//console.log("response RDV CREATED =>", response);
				});
			console.log('CREATED =>', collection)
			return collection
		}
	}
};
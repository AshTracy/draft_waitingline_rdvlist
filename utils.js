module.exports = { showHelp: showHelp };
const usage =
    "Usage: \n    add --type=['immediate', 'futur'] --at=DD-MM-YYYY.HH:mm \n    get --filter=['all', 'DD-MM-YYYY'] \n    watch \n    queue";

function showHelp() {
    console.log(usage);
    console.log('COMMANDS:');
    console.log('   -add: To add a new RDV');
    console.log("   -get: To display RDVs");
    console.log('   -watch: To add a link');
    console.log("   -queue: To sort the rdvs");
    console.log("   -info: Display the commands of the program");
    console.log('OPTIONS:');
    console.log('   -ADD:');
    console.log('       --type: type of RDV');
    console.log('       --at: time of RDV');
    console.log("   -GET:");
    console.log("       --filter: date of RDV");
    console.log("   -WATCH");
    console.log("   -QUEUE");
}
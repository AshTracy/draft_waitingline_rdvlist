const fs = require('fs')
const { upperCase } = require("lodash");
const moment = require('moment')

module.exports = {
    inputs: {
        rdvDate: {
            type: "ref",
            columnType: "datetime",
        },
        rdvType: {
            type: "string",
            isIn: ["immediate", "futur"],
        },
        name: {
            type: 'string'
        },
        filter: {
            type: 'string'
        }
    },
    exits: {},
    getData: function () { return getData() },
    filterDate: function (inputs) {
        return filterDate(inputs.rdvDate, getData())
    },
    displayData: function (data) { return displayData(data) },
    addData: function (inputs) {
        return addData(getData(), createRdv(inputs))
    }
}

function getData() {
    let fichier = fs.readFileSync('data.json')
    if (fichier.length !== 0) {
        let rdv = JSON.parse(fichier)
        return rdv
    } else
        return -1
}

function filterDate(date, data) {
    let dateRdvs = data;
    if (!date) {
        console.log('Next time enter a date!!')
        return -1
    }
    else if (date && date != 'all') {
        dateRdvs = data.filter(item => {
            if (moment(moment(item.consultation.startTimeRdv).format('DD-MM-YYYY'), 'DD-MM-YYYY').utc(true).toISOString() === moment(date, 'DD-MM-YYYY').utc(true).toISOString()) { return item } })
    }
    return dateRdvs
}

function displayData(data) {
    console.log(data)
}

function addData(data, rdv) {
    if (rdv === -1) {
        return -1
    }
    if (data === -1) {
        data = []
    }
    data.push(rdv)
    data = JSON.stringify(data)
    try {
        fs.writeFileSync('data.json', data)
        console.log('Rdv added successfully !')
    } catch (error) {
        return -1
    }
    return rdv
}

function createRdv(inputs) {
    let now = moment().utc(true).toISOString();
    let rdv = {
        consultation: {
            name: inputs.name ? inputs.name : 'None',
            createdAt: now,
            positionNumber: null,
            status: "PENDING",
            startTimeRdv: now,
            paymentStatus: "PENDING",
            rdvType: "IMMEDIATE",
            position: {
                hour: moment(now).add(-1, 'hours').format('HH:mm')
            }
        }
    };
    if (inputs.rdvType === 'immediate' || inputs.rdvType === 'futur') {
        rdv.consultation.rdvType = upperCase(inputs.rdvType);
	} else {
        console.log('Put a type !');
            return (-1)
	}
    if (inputs.rdvDate && inputs.rdvType === 'futur') {
        if (moment(inputs.rdvDate, 'DD-MM-YYYY.HH:mm').valueOf() < moment(now).add(-1, "hours").valueOf()) {
          console.log("\nImpossible to create a RDV at this date! Insert a validate date!\n");
          return -1;
        }
		rdv.consultation.startTimeRdv = moment(inputs.rdvDate, 'DD-MM-YYYY.HH:mm').utc(true)
			.toISOString();
		rdv.consultation.position.hour = moment(inputs.rdvDate, 'DD-MM-YYYY.HH:mm').format('HH:mm')
	} else if (inputs.rdvType !== "immediate"){
        console.log("Put a date !")
		return (-1);
    }
    return (rdv)
}

import { Test, TestingModule } from '@nestjs/testing';
import { RdvsController } from './rdvs.controller';

describe('RdvsController', () => {
  let controller: RdvsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [RdvsController],
    }).compile();

    controller = module.get<RdvsController>(RdvsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});

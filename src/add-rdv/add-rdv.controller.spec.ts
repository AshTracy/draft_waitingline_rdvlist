import { Test, TestingModule } from '@nestjs/testing';
import { AddRdvController } from './add-rdv.controller';
import { AddRdvService } from './add-rdv.service';

describe('AddRdvController', () => {
  let controller: AddRdvController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [AddRdvController],
      providers: [AddRdvService],
    }).compile();

    controller = module.get<AddRdvController>(AddRdvController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});

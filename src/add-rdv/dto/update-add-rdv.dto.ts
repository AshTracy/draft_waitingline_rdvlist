import { PartialType } from '@nestjs/mapped-types';
import { CreateAddRdvDto } from './create-add-rdv.dto';

export class UpdateAddRdvDto extends PartialType(CreateAddRdvDto) {}

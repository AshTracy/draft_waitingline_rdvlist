import { Test, TestingModule } from '@nestjs/testing';
import { AddRdvService } from './add-rdv.service';

describe('AddRdvService', () => {
  let service: AddRdvService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [AddRdvService],
    }).compile();

    service = module.get<AddRdvService>(AddRdvService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});

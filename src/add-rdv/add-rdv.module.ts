import { Module } from '@nestjs/common';
import { AddRdvService } from './add-rdv.service';
import { AddRdvController } from './add-rdv.controller';

@Module({
  controllers: [AddRdvController],
  providers: [AddRdvService]
})
export class AddRdvModule {}

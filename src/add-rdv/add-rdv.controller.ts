import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { AddRdvService } from './add-rdv.service';
import { CreateAddRdvDto } from './dto/create-add-rdv.dto';
import { UpdateAddRdvDto } from './dto/update-add-rdv.dto';

@Controller('add-rdv')
export class AddRdvController {
  constructor(private readonly addRdvService: AddRdvService) {}

  @Post()
  create(@Body() createAddRdvDto: CreateAddRdvDto) {
    return this.addRdvService.create(createAddRdvDto);
  }

  @Get()
  findAll() {
    return this.addRdvService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.addRdvService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateAddRdvDto: UpdateAddRdvDto) {
    return this.addRdvService.update(+id, updateAddRdvDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.addRdvService.remove(+id);
  }
}

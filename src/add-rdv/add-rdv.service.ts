import { Injectable } from '@nestjs/common';
import { CreateAddRdvDto } from './dto/create-add-rdv.dto';
import { UpdateAddRdvDto } from './dto/update-add-rdv.dto';

@Injectable()
export class AddRdvService {
  create(createAddRdvDto: CreateAddRdvDto) {
    return 'This action adds a new addRdv';
  }

  findAll() {
    return `This action returns all addRdv`;
  }

  findOne(id: number) {
    return `This action returns a #${id} addRdv`;
  }

  update(id: number, updateAddRdvDto: UpdateAddRdvDto) {
    return `This action updates a #${id} addRdv`;
  }

  remove(id: number) {
    return `This action removes a #${id} addRdv`;
  }
}

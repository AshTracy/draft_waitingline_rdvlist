# WaitingLine_Rdv

## About the project

This API is designed to produce a waiting line out of the inserted RDVs. The waitingline is sorted by:
# his priority which is determined by the type of RDV
  -RDV of type immediate has a priority of 1
  -RDV of type futur have a priority of 0
  The lowest priority has the most high degree of priority
# and the date of the RDV


## Built With
  NestJS: https://docs.nestjs.com
  Prisma: https://docs.nestjs.com/recipes/prisma#prisma


## Getting Started
To launch the project you have to follow these simple steps:
  
  # Prerequisites
    -npm
    
  # Installation
    1-Clone the repo
    2-Install NPM packages
    3-Run npm run start
    4-To run the swagger test go to: http://localhost:3000/api

  # Usage
    Can be used to get the waitingline after inserting a list of rdvs 


# Contact
Ashley Avocanh - tracyavocanh@gmail.com

import {IsDateString, IsEmpty, IsIn, IsNotEmpty, IsNumber, IsNumberString, IsOptional, IsString } from "class-validator";
import { ApiProperty } from "@nestjs/swagger"

export class Delete_one {
  @IsNotEmpty()
  @IsNumberString()
  id: string;
}

export class AddToLine {
  @IsNumber()
  @ApiProperty({
    type: 'number',
    default: 0,
    example: 0
  })
  positionNumber: number;

  @IsDateString()
  @ApiProperty({
    type: 'number',
    description: 'Id of the rdv',
    example: 8,
  })
  rdvId: number;

  @IsString()
  @ApiProperty({
    example: 'Annabelle',
    type: 'string',
    description: 'Name of the RDV',
  })
  name: string;

  @IsString()
  @ApiProperty({
    example: '15:00',
    type: 'string',
    description: 'Hour of the rdv date',
  })
  hour: string;

}

export class Update {
  @IsOptional()
  @IsString()
  @ApiProperty({
    required: false,
    example: 'futur',
    type: 'string',
    enum: ['futur', 'immediate'],
    description: 'Set the rdv type: futur(later) or immediate(now)',
  })
  @IsIn(['futur', 'immediate'])
  type: string;

  @IsOptional()
  @IsString()
  @ApiProperty({
    required: false,
    example: 8,
    type: 'string',
    description: 'Id of the rdv',
  })
  externalRdvId: string;

  @IsOptional()
  @IsString()
  @ApiProperty({
    required: false,
    example: 'GOMED',
    type: 'string',
    description: 'provenance of RDV',
  })
  app: string;

  @IsOptional()
  @IsString()
  @ApiProperty({
    required: false,
    example: 'Annabelle',
    type: 'string',
    description: 'Name of the RDV',
  })
  name: string;

  @IsOptional()
  @IsString()
  @ApiProperty({
    required: false,
    example: '15:00',
    type: 'string',
    description: 'Hour of the rdv date',
  })
  hour: string;

  @IsOptional()
  @IsString()
  @ApiProperty({
    required: false,
    example: '5c98e1682c713d22f1374e1d',
    type: 'string',
    description: 'hospital',
  })
  hospital: string;
}

export class GetFilterdDate {
  @IsOptional()
  @IsDateString()
  @ApiProperty({
    required: false,
    example: '2021-12-07T00:00:00.000Z',
    type: 'string',
    description: 'Date of the rdvs to be fetched',
  })
  date: Date;
}

export class OrderList {
  @IsDateString()
  @IsNotEmpty()
  @ApiProperty({
    example: '2021-12-07T00:00:00.000Z',
    type: 'string',
    description: 'The date of the rdvs in the waitingline',
  })
  date: Date;
}
export class CreateRdv {
  @IsString()
  @IsNotEmpty()
  @ApiProperty({
    example: '5c98e1682c713d22f1374e1d',
    type: 'string',
    description: 'Put the name of the hospital',
  })
  hospital: string;

  @IsNotEmpty()
  @IsString()
  @ApiProperty({
    example: 'futur',
    type: 'string',
    enum: ['futur', 'immediate'],
    description: 'Set the rdv type: futur(later) or immediate(now)',
  })
  @IsIn(['futur', 'immediate'])
  type: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty({
    example: 'Alice',
    type: 'string',
    description: 'Put the name of the patient',
  })
  name: string;

  @IsOptional()
  @ApiProperty({
    example: '2021-12-07T18:00:00.000Z',
    type: 'string',
    format: 'date',
    description: 'Put the date of the rdv',
  })
  @IsNotEmpty()
  date: Date;

  @IsString()
  @ApiProperty({
    example: 'ffld1slss1aa23aa3s',
    type: 'string',
    description: 'Put the id of rdv of the application',
  })
  @IsNotEmpty()
  externalRdvId: string;

  @IsString()
  @ApiProperty({
    example: 'GOMED',
    type: 'string',
    description: 'Put the app name',
  })
  @IsNotEmpty()
  app: string;
}

export class ChangeStatus {
  @IsString()
  @ApiProperty({
    example: '12',
    type: 'string',
    description: 'Put id',
  })
  @IsNotEmpty()
  id: string;

  @ApiProperty({
    example: 'PENDING',
    type: 'string',
    enum: ['PENDING', 'TERMINATED'],
  })
  @IsIn(['PENDING', 'TERMINATED'])
  status: string;
}
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { PrismaService } from './prisma.service';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger'

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

const config = new DocumentBuilder()
  .setTitle('Waiting line')
  .setDescription('Enter RDVs and get a waiting line')
  .setVersion('1.0')
  .addTag('RDV')
  .addTag('Waiting line')
  .build();
const document = SwaggerModule.createDocument(app, config);
SwaggerModule.setup('api', app, document);

  const prismaService: PrismaService = app.get(PrismaService);
  prismaService.enableShutdownHooks(app)

  await app.listen(3000);
}
bootstrap();

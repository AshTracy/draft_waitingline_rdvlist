
import { Injectable } from '@nestjs/common';
import { PrismaService } from '../prisma.service';
import {
  Rdv,
  Prisma
} from '@prisma/client';

@Injectable()
export class RdvService {
  constructor(private prisma: PrismaService) {}

  async rdv(rdvWhereUniqueInput: Prisma.RdvWhereUniqueInput): Promise<Rdv | null> {
    return this.prisma.rdv.findUnique({
      where: rdvWhereUniqueInput,
    });
  }

  async rdvs(params: {
    skip?: number;
    take?: number;
    where?: Prisma.RdvWhereInput;
    orderBy?: Prisma.RdvOrderByWithRelationInput;
  }): Promise<Rdv[]> {
    const { skip, take, where, orderBy } = params;
    return this.prisma.rdv.findMany({
      skip,
      take,
      where,
      orderBy,
    });
  }

  async createRdv(data: Prisma.RdvCreateInput): Promise<Rdv> {
    console.log('createRdv service', data)
    return this.prisma.rdv.create({
      data,
    });
  }

  async updateRdv(params: {
    where: Prisma.RdvWhereUniqueInput;
    data: Prisma.RdvUpdateInput;
  }): Promise<Rdv> {
    const { where, data } = params;
    return this.prisma.rdv.update({
      data,
      where,
    });
  }

  async deleteRdv(where: Prisma.RdvWhereUniqueInput): Promise<Rdv> {
    return this.prisma.rdv.delete({
      where,
    });
  }
}

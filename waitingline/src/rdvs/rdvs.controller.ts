import {
  Controller,
  Get,
  Param,
  Post,
  Body,
  Put,
  Delete,
  ValidationPipe,
  Query,
} from '@nestjs/common';
import { RdvService } from './rdv.service';
import { Rdv as RdvModel } from '@prisma/client'

import moment = require("moment");
import { ApiTags } from '@nestjs/swagger';
import { GetFilterdDate, Update, CreateRdv, ChangeStatus } from 'src/app.entity';
moment(new Date())

@ApiTags('RDV')
@Controller('rdvs')
export class RdvController {
  constructor(private rdvService: RdvService) {}

  @Get(':id')
  async getRdvById(@Param('id') id: string): Promise<RdvModel> {
    return this.rdvService.rdv({ id: Number(id) });
  }

  @Get()
  async getPendingRdvs(
    @Query(ValidationPipe) query: GetFilterdDate,
  ): Promise<RdvModel[]> {
    console.log('date', moment(query.date).utc(true).add(1, 'days').toDate());
    return this.rdvService.rdvs({
      where: {
        status: 'PENDING',
        date: query.date
          ? {
              gte: moment(query.date).utc(true).toDate(),
              lte: moment(query.date).utc(true).add(1, 'day').toDate(),
            }
          : undefined,
      },
    });
  }

  @Post()
  async createRdv(
    @Body(ValidationPipe)
    query: CreateRdv,
  ): Promise<RdvModel> {
    try {
      if (
        query.type !== ('futur' || 'immediate') ||
        moment(query.date).valueOf() < moment().valueOf()
      ) {
        //return an error
      }
      let { hospital, name, type, date, externalRdvId, app } = query;

      if (type === 'immediate') {
        date = moment().utc(true).toDate();
      }
      type = type.toUpperCase();
      let priority,
        positionNumber = 0;
      let hour = moment(query.date).add(-1, 'hour').format('HH:mm');

      date = query.date;

      if (type === 'FUTUR') {
        priority = 0;
      } else if (type === 'IMMEDIATE') {
        priority = 1;
      }

      const created = await this.rdvService.createRdv({
        name,
        type,
        date,
        hospital,
        priority,
        positionNumber,
        hour,
        externalRdvId,
        app,
      });
      return created;
    } catch (error) {
      console.log('add RDV error', error);
      return error;
    }
  }

  @Put(':id/status/:status')
  async changeStatus(@Param(ValidationPipe) params: ChangeStatus): Promise<RdvModel> {
    let created = await this.rdvService.updateRdv({
      data: {status: params.status},
      where: { id: Number(params.id) },
    });
    return created;
  }

  @Put(':id')
  async updateRdv(
    @Param('id') id: string,
    @Body(ValidationPipe) query: Update,
  ): Promise<RdvModel> {
    console.log('status =>', query);
    let created = await this.rdvService.updateRdv({
      where: { id: Number(id) },
      data: query,
    });
    return created;
  }

  @Delete(':id')
  async deleteRdv(@Param('id') id: string): Promise<RdvModel> {
    return await this.rdvService.deleteRdv({ id: Number(id) });
  }
}
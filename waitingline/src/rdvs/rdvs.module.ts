import { Module } from '@nestjs/common';
import { RdvController } from './rdvs.controller';
import { RdvService } from './rdv.service';
//import { AppController } from '../app.controller';
import { AppService } from '../app.service';
import { PrismaService } from '../prisma.service';
RdvService
RdvController

@Module({
    controllers: [RdvController],
    providers: [AppService,RdvService, PrismaService],
    exports: [RdvService, PrismaService]
})

export class RdvsModule {
    constructor(private rdvService: RdvService) {}
}
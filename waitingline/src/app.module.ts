import { Module } from '@nestjs/common';
import { RdvsModule } from './rdvs/rdvs.module';
import { WaitingLineModule } from './waitingLine/waitingline.module';
import { waitinglineController } from './waitingLine/waitingline.controller'
import { RdvController } from './rdvs/rdvs.controller';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { PrismaService } from './prisma.service';

@Module({
  imports: [RdvsModule, WaitingLineModule],
  controllers: [AppController, RdvController, waitinglineController],
  providers: [PrismaService, AppService],
})
export class AppModule {}

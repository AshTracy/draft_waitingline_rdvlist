import {
  Controller,
  Get,
  Param,
  ValidationPipe,
  Query,
} from '@nestjs/common';
import { WaitinglineService } from './waitingline.service';
import { Rdv as RdvModel } from '@prisma/client'
import moment = require("moment");
import { ApiTags } from '@nestjs/swagger';
import { GetFilterdDate } from '../app.entity';
const { orderBy } = require('lodash');
moment(new Date())


@ApiTags('Waiting line')
@Controller('waitinglines')
export class waitinglineController {
  constructor(private waitingLineService: WaitinglineService) { }
  
  @Get('/:id')
  async getRdvById(@Param('id') id: string) {
    let RDV = await this.waitingLineService.waitingline({ id: Number(id) });
    if (RDV.positionNumber == 0) {
      let rdv = sortRdvs(
        await this.waitingLineService.waitinglines({
          where: {
            id: RDV.id
          }, //SET AN APRROPRIATE FORMAT TO DATE
        }),
      );
      RDV = rdv[0]
    }
    return {name: RDV.name, hour: RDV.hour, positionNumber: RDV.positionNumber, priority: RDV.priority} ;
  }

  @Get('hospital/:hospital')
  async getPendingRdvs(@Param('hospital') hospital: string, @Query(ValidationPipe) query: GetFilterdDate): Promise<RdvModel[]> {
    /*console.log('date', moment(
      moment(query.date).format('YYYY-MM-DD')).utc(true).toDate(),
      moment(query.date).utc(true).add(1, 'day').toDate(),
    );*/
    let rdvs = await this.waitingLineService.waitinglines({
      orderBy: {
        date: 'asc',
      },
      where: {
        date: query.date
          ? {
              gte:  moment(moment(query.date).format('YYYY-MM-DD')).utc(true).toDate(),
              lt: moment(moment(query.date).format('YYYY-MM-DD')).utc(true).add(1, 'day').toDate(),
            }
          : {
              gte: moment(moment(query.date).format('YYYY-MM-DD')).toDate(),
              lt: moment(moment(query.date).format('YYYY-MM-DD')).add(1, 'day').toDate(),
            },
        hospital: hospital,
      }, //SET AN APRROPRIATE FORMAT TO DATE
    });
    let sortedRdvs = await sortRdvs(rdvs)
      rdvs.forEach(async (elem) => {
        await this.waitingLineService.update({
          where: { id: elem.id },
          data: { positionNumber: elem.positionNumber }
        })
      }
    );
    return sortedRdvs.sort((a, b) => a.positionNumber - b.positionNumber);
  }
}

async function sortRdvs(rdvs) {
  let pendingRdvs = rdvs,
    positionNumber = 1,
    finalRdvs = [], c = 0;

  //SORT BY PRIORITY
  pendingRdvs = orderBy(pendingRdvs, ['date', 'priority'], ['asc', 'asc']);
  //SORT THE PRIORITY

  //GIVE THE NUMBER POSITION
  for (const r of pendingRdvs) {
    r.positionNumber = positionNumber;
    if (r.status === 'PENDING') {
      finalRdvs.push({ name: r.name, hour: r.hour, positionNumber: r.positionNumber, hospital: r.hospital, rdvBefore: c, externalRdvId: r.externalRdvId });
      c++;
    }
    positionNumber++;
  }
  //console.log('final rdvs =>', finalRdvs);
  return finalRdvs;
  //GIVE THE NUMBERS POSITION
}
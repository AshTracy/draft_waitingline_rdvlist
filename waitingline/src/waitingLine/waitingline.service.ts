import { Injectable } from '@nestjs/common';
import { PrismaService } from '../prisma.service';
import {
  Prisma,
  Rdv
} from '@prisma/client';

@Injectable()
export class WaitinglineService {
  constructor(private prisma: PrismaService) {}

  async waitingline(
    postWhereUniqueInput: Prisma.RdvWhereUniqueInput,
  ): Promise<Rdv | null> {
    return this.prisma.rdv.findUnique({
      where: postWhereUniqueInput,
    });
  }

  async waitinglines(params: {
    skip?: number;
    take?: number;
    where?: Prisma.RdvWhereInput;
    orderBy?: Prisma.RdvOrderByWithRelationInput;
  }): Promise<Rdv[]> {
    const { skip, take, where, orderBy } = params;
    return this.prisma.rdv.findMany({
      skip,
      take,
      where,
      orderBy,
    });
  }

  async update(params: {
    where: Prisma.RdvWhereUniqueInput;
    data: Prisma.RdvUpdateInput;
  }): Promise<Rdv> {
    const { where, data } = params;
    return this.prisma.rdv.update({
      data,
      where,
    });
  }
}

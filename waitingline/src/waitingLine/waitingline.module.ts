import { Module } from '@nestjs/common';
import { waitinglineController } from './waitingline.controller';
import { WaitinglineService } from './waitingline.service';;
import { AppService } from '../app.service';
import { PrismaService } from '../prisma.service';
WaitinglineService
waitinglineController

@Module({
    controllers: [waitinglineController],
    providers: [AppService, WaitinglineService, PrismaService],
    exports: [WaitinglineService, PrismaService]
})

export class WaitingLineModule {
    constructor(private waitingLineService: WaitinglineService) {}
}
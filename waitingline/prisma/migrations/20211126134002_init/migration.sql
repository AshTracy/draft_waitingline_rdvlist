/*
  Warnings:

  - Added the required column `app` to the `Rdv` table without a default value. This is not possible if the table is not empty.
  - Added the required column `externalRdvId` to the `Rdv` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "Rdv" ADD COLUMN     "app" TEXT NOT NULL,
ADD COLUMN     "externalRdvId" TEXT NOT NULL;

/*
  Warnings:

  - You are about to drop the `Post` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `User` table. If the table is not empty, all the data it contains will be lost.

*/
-- DropForeignKey
ALTER TABLE "Post" DROP CONSTRAINT "Post_authorId_fkey";

-- DropTable
DROP TABLE "Post";

-- DropTable
DROP TABLE "User";

-- CreateTable
CREATE TABLE "WaitingLine" (
    "id" SERIAL NOT NULL,
    "name" TEXT NOT NULL,
    "hour" TEXT NOT NULL,
    "positionNumber" INTEGER NOT NULL,

    CONSTRAINT "WaitingLine_pkey" PRIMARY KEY ("id")
);

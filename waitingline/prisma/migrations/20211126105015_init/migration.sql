/*
  Warnings:

  - You are about to drop the column `RdvType` on the `Rdv` table. All the data in the column will be lost.
  - You are about to drop the `WaitingLine` table. If the table is not empty, all the data it contains will be lost.
  - Added the required column `hospital` to the `Rdv` table without a default value. This is not possible if the table is not empty.
  - Added the required column `type` to the `Rdv` table without a default value. This is not possible if the table is not empty.

*/
-- DropForeignKey
ALTER TABLE "WaitingLine" DROP CONSTRAINT "WaitingLine_rdvId_fkey";

-- AlterTable
ALTER TABLE "Rdv" DROP COLUMN "RdvType",
ADD COLUMN     "hospital" TEXT NOT NULL,
ADD COLUMN     "type" TEXT NOT NULL;

-- DropTable
DROP TABLE "WaitingLine";

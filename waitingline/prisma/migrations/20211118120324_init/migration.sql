/*
  Warnings:

  - Added the required column `rdvId` to the `WaitingLine` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "WaitingLine" ADD COLUMN     "rdvId" INTEGER NOT NULL;

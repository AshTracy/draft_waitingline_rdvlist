const yargs = require('yargs')
const axios = require("axios");
const myArgs = process.argv.slice(2);
const http = require("http");
const hostname = "127.0.0.1";
const port = 3000;
const md5 = require("md5");


const waitingLine = require('./waitingLine');
const utils = require("./utils");
const getDayRdv = require('./rdvs');
const fs = require("fs");

const argv = yargs.command(
  "add",
  "Adds a new rdv to the database",
  {
    type: {
      description: "the type of rdv",
      alias: "t",
      type: "string",
    },
    at: {
      desciption: "date of the rdv",
      alias: "a",
      type: "date",
    },
    name: {
      description: "name of patient",
      alias: "n",
      type: "string"
    }
  }).command("get", "get rdvs in the database", {
    filter: {
      description: "the date of the rdvs",
      alias: "f",
      type: "string",
    },
    at: {
      desciption: "date of the rdv",
      type: "date",
    },
  }).command("watch", 'whatch the commands').help().alias('help', 'h').command("queue", "sort the rdvs of today", {}).command("info", "return informations on the programm", {}).argv
  ;

  const server = http.createServer((req, res) => {
    let data = "";
    req.on("data", (chunk) => {
      data += chunk;
    });
    req.on("end", (res) => {
      console.log(data);
    });

    if (req.url === "/info") {
      console.log("info--");
    }
    res.setHeader("Content-Type", "text/plain");

    res.end(`Hello world`);
  });

if (myArgs) {
  if (myArgs.indexOf("watch") >= 0) {
    server.listen(port, hostname, () => {
      console.log(`Server running at http://${hostname}:${port}/`);
    });
  }
  else {
    commands().then((res) => {
      axios.post("http://127.0.0.1:3000/info", res ).catch((err) => {});
    });
    return 0;
  }

  async function commands() {
    if (myArgs.indexOf("add") >= 0) {
      let rdvList = getDayRdv.addData({
        rdvType: `${argv.type ? argv.type : ""}`,
        rdvDate: `${argv.at ? argv.at : ""}`,
        name: argv.name
      });
      return rdvList;
    }
    if (myArgs.indexOf("queue") >= 0) {
      let data = getDayRdv.filterDate({
        rdvDate: `all`,
      });
      pendingRdvs = waitingLine.fn({ getRdv: data });
      let previous = null
      fs.watch('./data.json', (event, filename) => {
        let current = md5(fs.readFileSync('./data.json'));
        if (current !== previous) {
          console.clear();
          let data = getDayRdv.filterDate({
            rdvDate: `all`,
          });
          pendingRdvs = waitingLine.fn({ getRdv: data });
        }
        previous = current
      })
      return pendingRdvs
    }
    if (myArgs.indexOf("get") >= 0) {
          console.log('filter', argv.filter)
        let rdvs = getDayRdv.filterDate({
        rdvDate: `${argv.filter ? argv.filter : ""
          }`,
        filter: "true",
      });
      getDayRdv.displayData(rdvs)
      return rdvs;
    }
    if (myArgs.indexOf("info") >= 0) {
      utils.showHelp();
      return
    }
  }
}